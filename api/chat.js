import {Http} from './../utils';

//import { chatHistoryUrl, fileServiceUrl, presenceUrl } from './../config/baseUrls';

import {configs} from './../config';

export const getRecentConversations = async (user_id, token) => {
  return await Http.doRequest(
    `${configs.selected.chatHistoryUrl}/api/v1/conversations/${user_id}`,
    'GET',
    {authorization: `Bearer ${token}`},
  );
};

export const getChatHistory = async (
  token,
  from_user_id,
  to_user_id,
  unix_time = Date.now(),
  limit = 100,
  type = 0,
) => {
  let apiUrl = `${configs.selected.chatHistoryUrl}/api/v1/conversation/messages/before?type=${type}&dateFrom=${unix_time}&from=${from_user_id}&to=${to_user_id}&limit=${limit}`;
  return await Http.doRequest(apiUrl, 'GET', {
    authorization: `Bearer ${token}`,
  });
};

export const uploadFile = async (
  token,
  user_id,
  file,
  organization_id,
  interaction_id,
  conv_id,
) => {
  return await Http.doFileUpload(
    `${configs.selected.fileServiceUrl}/upload`,
    {authorization: token},
    {
      file: file,
      organization_id: organization_id,
      interaction_id: interaction_id,
      uploaded_by: user_id,
      conv_id: conv_id,
    },
  );
};

export const getPresence = async (token, state) => {
  let apiUrl = `${configs.selected.presenceUrl}/presence/tetherfi?limit=100`;
  if (state) {
    apiUrl = `${apiUrl}&state=${state}`;
  }
  return await Http.doRequest(apiUrl, 'GET', {
    authorization: `Bearer ${token}`,
  });
};

//https://tetherfi.cloud:25213/api/v1/conversation/update/0000000070f261360170f26151030001?from=00000000713445b701716cd382e20028&mid=5268c668-c4dd-4a33-ac2a-cb578db94d3a&isStarred=false
export const starMsg = async (
  token,
  from_user_id,
  my_user_id,
  mid,
  is_starred,
) => {
  let apiUrl = `${configs.selected.chatHistoryUrl}/api/v1/conversation/update/${my_user_id}?from=${from_user_id}&mid=${mid}&isStarred=${is_starred}`;
  return await Http.doRequest(apiUrl, 'PUT', {
    authorization: `Bearer ${token}`,
  });
};
