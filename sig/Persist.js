import { Logger } from './logger.js';
import {storeItem, retrieveItem, clear} from './../utils/LocalStorage';

class Persist {
    constructor (session) {
        this.session = session;
    }

    storeSession = () => {
        storeItem("livechat.session", JSON.stringify(this.session));
    }

    updateSent = () => {
        storeItem("livechat.lsent", this.session.mid.toString());
    }

    updateReceived = () => {
        storeItem("livechat.lreceived", this.session.lastRecvMid.toString());
    }

    loadSession = () => {
        var ss = retrieveItem("livechat.session");

        if (!ss) 
            return false;

        try { 
            this.session = JSON.parse(ss); 
        }
        catch (e) { 
            return false; 
        }

        var basic = this.session.sessionID &&
            this.session.auth &&
            this.session.userid &&
            (!this.session.bEndReceived);

        if (!basic) {
            Logger.info("Basic session loading failed");
            return false;
        }

        var sent = retrieveItem("livechat.lsent");
        var received = retrieveItem("livechat.lreceived");

        if (!(sent && received)) {
            Logger.warn("mids not found");
            return false;
        }

        this.session.mid = parseInt(sent);
        this.session.lastRecvMid = parseInt(received);

        if (!(this.session.mid >= 0 && this.session.lastRecvMid >= 0)) {
            Logger.warn("mids are not ints");
            return false;
        }

        return true;
    }

    clear = () => {
        Logger.debug(`Removing Session Storage for Persist`);
        storeItem("livechat.session",  "" );
        storeItem("livechat.lsent", "" );
        storeItem("livechat.lreceived", "" );
    }

}

export { Persist };