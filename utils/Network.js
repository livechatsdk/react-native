import {NetInfo} from 'react-native';

const getNetworkConnectivity = async () => {
  console.log('### FNetworkUtils :: getNetworkConnectivity');
  let connection_status = false;
  await NetInfo.isConnected.fetch().then(isConnected => {
    console.log('### FNetworkUtils :: ' + (isConnected ? 'online' : 'offline'));
    connection_status = isConnected;
  });
  console.log(
    '### FNetworkUtils :: getNetworkConnectivity => ',
    connection_status,
  );
  return connection_status;
};

export default {
  getNetworkConnectivity,
};
