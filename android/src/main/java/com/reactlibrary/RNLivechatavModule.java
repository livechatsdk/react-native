package com.reactlibrary;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.view.SurfaceView;

import com.facebook.react.bridge.ActivityEventListener;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.LifecycleEventListener;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import org.json.JSONException;
import org.json.JSONObject;

import sdk.livechat.DeviceInfo;
import sdk.livechat.Version;
import sdk.livechat.audio.AudioPlay;
import sdk.livechat.Configs;
import sdk.livechat.Log;
import sdk.livechat.RTCSession;
import sdk.livechat.Signalling;
import sdk.livechat.SignallingDelegate;
import sdk.livechat.video.VideoEventsListener;
import sdk.livechat.video.VideoSessionFactory;



public class RNLivechatavModule extends
        ReactContextBaseJavaModule implements LifecycleEventListener, ActivityEventListener {

  private RTCSession rtcSession;
  private Signalling sigSession;
  private String appName;
  private DeviceEventManagerModule.RCTDeviceEventEmitter jsEventEmitter;
  private String sessionId;
  private Callback startAvComplete;
  private Callback initAvComplete;

  private final ReactApplicationContext reactContext;

  VideoEventsListener videoEventsListener = new VideoEventsListener() {

      private void emitVideoEvent(int index, String type, String streamId )
      {
          WritableMap map = Arguments.createMap();
          map.putInt( "index", index );
          map.putString( "type", type  );
          map.putString( "streamId", streamId );
          jsEventEmitter.emit( "onvideo", map );
      }

      private void emitVideoDropEvent(int index, String type, String streamId )
      {
          WritableMap map = Arguments.createMap();
          map.putInt( "index", index );
          map.putString( "type", type  );
          map.putString( "streamId", streamId );
          jsEventEmitter.emit( "onvideodrop", map );
      }

      @Override
      public void onLocalVideo(SurfaceView surfaceView, String streamId ) {
          surfaceView.setZOrderMediaOverlay(true);
          int ret = RNVideoViewManager.Instance.addSurfaceView( surfaceView, "local" );
          emitVideoEvent( ret, "local", streamId );
      }

      @Override
      public void onRemoteVideoAdded(SurfaceView surfaceView, String streamId ) {
          surfaceView.setZOrderOnTop( false );
          int ret = RNVideoViewManager.Instance.addSurfaceView( surfaceView, "remote" );
          emitVideoEvent( ret, "remote", streamId );
      }

      @Override
      public void onRemoteVideoRemoved(SurfaceView surfaceView, String streamId ) {
          int ret = RNVideoViewManager.Instance.removeSurfaceView( surfaceView );
          emitVideoDropEvent( ret, "remote", streamId );
      }

      @Override
      public void onScreenPermissionSuccess() {
      }

      @Override
      public void onScreenPermissionFailed() {
      }
  };

  // ActivityEventListener

    @Override
    public void onActivityResult( Activity activity, int requestCode, int resultCode, Intent data)
    {
        if( requestCode != VideoSessionFactory.SCREEN_PERM_REQUEST_CODE )
        {
            return;
        }

        Log.d( "calling setActivityResult, resultCode = " + resultCode );
        boolean ret = VideoSessionFactory.Instance.setActivityResult( requestCode, resultCode, data );
        Log.i( "Complete initav" );
        this.initAvComplete.invoke(ret, ret ? "" : "Screen permission failed" );
    }

    @Override
    public void onNewIntent(Intent intent) {

    }

    //

  public RNLivechatavModule(ReactApplicationContext reactContext) {
      super(reactContext);
      this.reactContext = reactContext;
      
      this.reactContext.addActivityEventListener( this );
      appName = "unassigned";
  }

  @Override
  public void initialize() {
      super.initialize();
      jsEventEmitter = reactContext.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class);
	this.reactContext.addLifecycleEventListener( this );
  }

  @Override
  public String getName()
  {
      return "RNLivechatav";
  }

  @ReactMethod
  public void setAppName( String name )
  {
      appName = name;
  }

  @ReactMethod
  public void getAppName( Promise p )
  {
      p.resolve( appName );
  }

  @ReactMethod
  public void initav( String sid, String avmode, final Callback onComplete )
  {

      Log.d( "native initav" );

      this.initAvComplete = onComplete;


      try {
          Configs.Instance.avmode = Configs.AVMode.valueOf(avmode.toUpperCase());
      }
      catch ( Exception ex ){
          Log.w( "avmode was not set : " + ex.getMessage() );
      }

      //if( Configs.Instance.avmode != Configs.AVMode.AUDIO )
      //{
          VideoSessionFactory.Instance.initialize( videoEventsListener, this.getCurrentActivity() );

          if( Configs.Instance.avmode == Configs.AVMode.SCREEN )
          {
              Log.i( Configs.Instance.avmode + " mode, initav deferred" );
              return;
          }
      //}

      onComplete.invoke( true );

  }

  @ReactMethod
  public void startav(String sid,  String avmode, final Callback onComplete )
  {
      Log.i( "native startav " );

      if( rtcSession != null )
      {
          Log.w( "Session in progress, call 'endav' to end, startav ignored" );
          onComplete.invoke( false, "SessionExist" );
          return;
      }

      this.sessionId = sid;
      this.startAvComplete = onComplete;

      rtcSession = new RTCSession( reactContext );

      sigSession  = new Signalling(){

        @Override
        public void sendSigMessage( String s )
        {
            WritableMap map = Arguments.createMap();
            map.putString( "msg", s );
            jsEventEmitter.emit( "sigmsg", map );
        }

        @Override
        public void init( SignallingDelegate signallingDelegate, String s ) {
              // TODO
        }


        @Override
        public void close() {
            // TODO
        }
      };

      rtcSession.setStartParams( false );

      if( ! this.startav_continue( sid, onComplete ) )
      {
          onComplete.invoke( false, "StartAvInitError" );
      }
  }

   boolean startav_continue( String sid, final Callback onComplete  )
   {
       return rtcSession.start( sigSession, sid, new RTCSession.RTCEventsHandler() {

           @Override
           public void onStartSuccess() {
               onComplete.invoke(true, null );
           }

           @Override
           public void onStartFailure(Exception e) {
               onComplete.invoke( false, ( ( e == null ) ? "" : e.getMessage()) );
           }

           @Override
           public void onDisconnect()
           {
               jsEventEmitter.emit( "ondisconnect", Arguments.createMap() );
           }

           @Override
           public void onReconnect()
           {
               jsEventEmitter.emit( "onreconnect", Arguments.createMap() );
           }

           @Override
           public void onReconnectableEnd()
           {
               jsEventEmitter.emit( "onreconnend", Arguments.createMap() );
           }

           @Override
           public void onEnd(EndType endType, String s)
           {
               WritableMap map = Arguments.createMap();
               map.putString( "type", endType.toString() );
               map.putString( "desc", s );
               jsEventEmitter.emit( "onend", map );
           }

       } );
   }


  @ReactMethod void tryReconnect()
  {
      rtcSession.reNegotiate();
  }


  @ReactMethod void getDeviceInfo( Callback result )
  {
      result.invoke( DeviceInfo.getInfo() );
  }


  @ReactMethod
  public void passSigMessage( String message )
  {
      if( rtcSession == null ) {
          Log.w("RTCSession is null, sigmessage ignored" );
          return;
      }

    rtcSession.onSigMessage( message );
  }

  @ReactMethod
  public void endav( String reason )
  {
      if( rtcSession == null ) {
          Log.w("RTCSession is null, endav with reason '" + reason + "' ignored"  );
          return;
      }

      RNVideoViewManager.Instance.reset();

      rtcSession.end( reason );
      rtcSession = null;
  }

  @ReactMethod
  public void setConfig( String configs )
  {
      try {
          JSONObject job = new JSONObject( configs );
          Configs.Instance.configByJSON( job );
          Configs.Instance.setPauseAudio( AppConfigs.Instance.PauseMusicRsid );
      } catch (JSONException e) {
          Log.e( "Invalid config message", e );
      }
  }

  @ReactMethod
  public void pause()
  {
      if( rtcSession == null ) {
          Log.w("RTCSession is null, pause request ignored"  );
          return;
      }

    rtcSession.pause( true );
  }

  @ReactMethod
  public void resume()
  {
      if( rtcSession == null ) {
          Log.w("RTCSession is null, resume request ignored"  );
          return;
      }
    rtcSession.resume();
  }

  @ReactMethod
  public void muteMic( boolean enable )
  {
      if( rtcSession == null ) {
          Log.w("RTCSession is null, muteMic(" + enable + ") request ignored"  );
          return;
      }

      rtcSession.muteMic( enable );
  }

  @ReactMethod
  public void speakerPhone( boolean enable )
  {
      if( rtcSession == null ) {
          Log.w("RTCSession is null, speakerPhone(" + enable + ") request ignored"  );
          return;
      }

      rtcSession.changeSpeaker( enable );
  }

  @ReactMethod
  public void playAudio()
  {
      AudioPlay.createAndPlay( reactContext );
  }

  @ReactMethod
  public void stopAudio()
  {
      AudioPlay.stopAndDestroy();
  }

  private void emitLogEvent( String type, String tag, String sid, String msg, String time )
  {
        WritableMap map = Arguments.createMap();
        map.putString( "type", type );
        map.putString( "sid", sid );
        map.putString( "class", tag );
        map.putString( "msg", msg );
        map.putString( "time", time );
        jsEventEmitter.emit( "logmsg", map );
  }

  @ReactMethod
  public void enableLogEvents( boolean enable )
  {
      //onLogMsgCallback = log;
      if( !enable )
      {
          Log.SetLogger( new sdk.livechat.logger.LvLogger() );
          return;
      }

      Log.SetLogger(new sdk.livechat.logger.LvLogger() {

          @Override
          public void i(String tag, String sid, String msg, String time)
          {
              super.i( tag, sid, msg, time );
              emitLogEvent( "Info", tag, sid, msg, time );
          }

          @Override
          public void d(String tag, String sid, String msg, String time)
          {
              super.d( tag, sid, msg, time );
              emitLogEvent( "Debug", tag, sid, msg, time );
          }

          @Override
          public void e(String tag, String sid, String msg, String time)
          {
              super.e( tag, sid, msg, time );
              emitLogEvent( "Error", tag, sid, msg, time );
          }

          @Override
          public void w(String tag, String sid, String msg, String time)
          {
              super.w( tag, sid, msg, time );
              emitLogEvent( "Warn", tag, sid, msg, time );
          }

      });
  }

  @ReactMethod
  public void getLibVersion( Callback cb )
  {
        cb.invoke( Version.getVersionStr() );
  }

  @ReactMethod
  public void echo( String msg, Callback cb )
  {
      cb.invoke( msg + " received" );
  }

  @ReactMethod
  public void pauseVideo( boolean includeRemote, boolean byUser )
  {
      if( rtcSession == null )
      {
          Log.w( "RTCSession is nil, pauseVideo(" + includeRemote + ") request ignored" );
          return;
      }
      rtcSession.pauseVideo( includeRemote, byUser );
  }

  @ReactMethod
  public void resumeVideo( boolean includeRemote, boolean byUser )
  {
      if( rtcSession == null )
      {
          Log.w( "RTCSession is nil, resumeVideo(" + includeRemote + ") request ignored" );
          return;
      }
      rtcSession.resumeVideo( includeRemote, byUser );
  }


  @ReactMethod
  public void getui( Callback cb )
  {
      SurfaceView sf = new TestSurfaceView( reactContext );
      sf.setBackgroundColor(Color.RED );
      int pos = RNVideoViewManager.Instance.addSurfaceView( sf, "remote" );
      //cb.invoke( pos );
  }


  // LifecycleEventListener

    @Override
    public void onHostResume()
    {
        Log.d( "onResume" );
        //VideoSessionFactory.Instance.resumeVideo();

        jsEventEmitter.emit( "onForeground", Arguments.createMap() );

        if( rtcSession == null )
        {
            Log.d( "no rtc session" );
            return;
        }


        if( Configs.Instance.avmode != Configs.AVMode.SCREEN )
        {
            rtcSession.resumeVideo(); // resumeVideo( true, false );
        }
    }

    @Override
    public void onHostPause() {

        Log.d( "onPause" );

        jsEventEmitter.emit( "onBackground", Arguments.createMap() );

        //VideoSessionFactory.Instance.pauseVideo();
        if( rtcSession == null )
        {
            Log.d( "no rtc session" );
            return;
        }

        if( Configs.Instance.avmode != Configs.AVMode.SCREEN )
        {
            rtcSession.pauseVideo();
        }
    }

    @Override
    public void onHostDestroy() {

        Log.d( "onDestroy" );

        VideoSessionFactory.Instance.pauseVideo();
    }

    // Below 2 listeners are required for React Native 0.65+ to fix this issue https://issueexplorer.com/issue/facebook/react-native/32051
    @ReactMethod
    public void addListener(String eventName) {
      // Keep: Required for RN built in Event Emitter Calls.
    }
  
    @ReactMethod
    public void removeListeners(Integer count) {
      // Keep: Required for RN built in Event Emitter Calls.
    }

    // End of LifecycleEventListener

}

