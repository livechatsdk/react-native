import AvChat from './av/LivechatAv'
import {sdkVersion} from './sig/version'

export const VERSION = {
  PACKAGE : "3.0.0",
  GetAV : AvChat.getLibVersion,
  SIG : sdkVersion
};


/*

Usage :

import VERSION from 'VERSION'

VERSION.PACKAGE;
VERSION.GetAV( function(versionStr){  } );
VERSION.SIG;

// All returned value components are strings

*/


