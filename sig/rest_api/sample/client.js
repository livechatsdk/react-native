//You need to import livechat2api which is the initial contact point for the rest api
import { Livechat2Api } from "../../livechat2api";

/*
The config object to set up api urls need to be passed as an argument on SDK initialization and this can be imported
from a file, please note that the key names need to be same and you can use the sample config and integrate it to your application.
*/
import * as config from "./config";
import { Logger } from '../../logger';

//initializing SDK
let liveChatClient = new Livechat2Api(config.webSocketUrl, config);

/*
liveChatClient instance contains an object named "api"
the api object returns 5 instances to different rest api's the SDK support currently, as listed below

api.presence
api.contacts
api.user_accounts
api.file
api.conversations

each of these instances contain their own set of operatioms defined as functions.

You can find these api methods for each api under the rest_api directory and documentation related to each method

*/


let userId = "2c92809c71d5213c0172072e7d490027";

//Sample of calling getUserConversation method defined in conversations api
//Most of the api's support ES6 Promises and details are documented in each api
liveChatClient.api.conversations.getUserConversations(userId).then(conversationData => {
    Logger.info(JSON.stringify(conversationData));
}).catch(e => {
    Logger.error(`Error in getUserConversations`, e);
});