
#import "RNLivechatav.h"
#import "SigImpl.h"
#import "AppConfigs.h"
#import "RNVideoViewManager.h"
#import "Version.h"

#import <Configs.h>
#import <RTCSession.h>
#import <Log.h>
#import <DeviceInfo.h>
#import <AudioPlay.h>
#import <VideoEventsListener.h>
#import <VideoSessionFactory.h>

#import <UIKit/UIKit.h>
#import <UIKit/UIApplication.h>


@interface RNLivechatav()
{
    RTCSession* rtcSession;
    NSString* appName;
    VideoEventsListener* videoEventsListener;
}

@end

@implementation RNLivechatav


RCT_EXPORT_MODULE();

-(instancetype) init
{
    self = [super init];
    appName = @"unassigned";
    videoEventsListener = [[VideoEventsListener alloc] init];
    
    videoEventsListener.onLocalVideo = ^(UIView* view, NSString* streamId )
    {
        int ret = [[RN_VideoViewManager Instance] addVideoView:view type:@"local"];
        [self emitVideoEvent:@"onvideo" index:ret type:@"local" streamId:streamId];
        // dispatch_async(dispatch_get_main_queue(), ^{
        // });
    };
    
    videoEventsListener.onRemoteVideoAdded = ^(ViewEvent* event, NSString* streamId )
    {
        dispatch_async(dispatch_get_main_queue(), ^{
       
                int ret = [[RN_VideoViewManager Instance] addVideoView:[event createView] type:@"remote"];
            [self emitVideoEvent:@"onvideo" index:ret type:@"remote" streamId:streamId];
            }
        );
    };
    
    videoEventsListener.onRemoteVideoRemoved = ^(UIView *view, NSString *streamId)
    {
        logdp( @"View removed invoked with stream %@", streamId);
        int ret = [[RN_VideoViewManager Instance] removeVideoView:view];
        logdp( @"Emiting event, Index of the view being removed %d", ret);
        [self emitVideoEvent:@"onvideodrop" index:ret type:@"remote" streamId:streamId ];
    };
    
    videoEventsListener.onResetTracks = ^()
    {
        
    };
    
    return self;
}

+ (BOOL)requiresMainQueueSetup
{
    return NO;
}

- (dispatch_queue_t)methodQueue
{
    return dispatch_get_main_queue();
}

- (NSArray<NSString *> *)supportedEvents
{
    return [NSArray arrayWithObjects:
            @"sigmsg", @"logmsg", @"onend", @"onvideo", @"ondisconnect", @"onreconnect", @"onreconnend", @"onvideodrop", nil ];
}

-(void)emitVideoEvent:(NSString*)name index:(int)index type:(NSString*)type streamId:(NSString*)streamId
{
    [self sendEventWithName:name body:
     @{
         @"index":[NSNumber numberWithInt:index],
         @"type" : type,
         @"streamId" : streamId
     }
    ];
}

/*
-(void)emitVideoDropEvent:(int)index type:(NSString*)type
{
    [self sendEventWithName:@"onvideodrop" body:
     @{
         @"index":[NSNumber numberWithInt:index],
         @"type" : type,
         @"streamId" : @"[na]"
     }
    ];
}
*/

/*
-(void)emitVideoEvent:(int)index type:(NSString*)type
{
    [self sendEventWithName:@"onvideo" body:@{ @"index":[NSNumber numberWithInt:index], @"type" : type } ];
}
*/

RCT_EXPORT_METHOD(setAppName:(NSString*)name)
{
    appName = name;
}

RCT_EXPORT_METHOD(getAppName:(RCTPromiseResolveBlock)resolve reject:(RCTPromiseRejectBlock)rejects)
{
    resolve( appName );
}

-(void) onBackGround
{
    logi( @"onBackground" );
    //[VideoSessionFactory.Instance pauseVideo];
    
    if( rtcSession == nil )
    {
        logw( @"RTCSession is nil, onBackGround ignored" );
        return;
    }
    
    [rtcSession pauseVideo];
}

-(void) onForeground
{
    logi( @"onForeground" );
    //[VideoSessionFactory.Instance resumeVideo];
    
    if( rtcSession == nil )
    {
        logw( @"RTCSession is nil, onForeground ignored" );
        return;
    }
    
    [rtcSession resumeVideo];
}

-(void) setNotifications
{
    [NSNotificationCenter.defaultCenter addObserver:self
     selector:@selector(onBackGround) name:UIApplicationWillResignActiveNotification object:nil];
    
    [NSNotificationCenter.defaultCenter addObserver:self
    selector:@selector(onForeground) name:UIApplicationDidBecomeActiveNotification object:nil];
}

RCT_EXPORT_METHOD(initav:(NSString*)sid avmode:(NSString*)avmode
                  onComplete:(RCTResponseSenderBlock)onComplete
                   )
{
    onComplete( [NSArray arrayWithObjects:[NSNumber numberWithBool:YES], nil] );
}


RCT_EXPORT_METHOD(startav:(NSString*)sid avmode:(NSString*)avmode
                  onComplete:(RCTResponseSenderBlock)onComplete
                   )
{
    [self setNotifications];
    
    if( rtcSession != nil )
    {
        loge( @"Session in progress, call 'endav' to end, startav failed" );
        onComplete( [NSArray arrayWithObjects: [NSNumber numberWithBool:NO], @"SessionExist", nil] );
        return;
    }
    
    rtcSession = [[RTCSession alloc] init];
    
    SigImpl* sig = [[SigImpl alloc] initWithOnMsgBlk: ^(NSString* outMsg)
                    {
                        
                        [self sendEventWithName:@"sigmsg" body:@{ @"msg": outMsg } ];
                        
                    } ];
    
    if( [avmode isEqualToString: @"video"] )
    {
       Configs.Instance.avmode = AVModeVideo;
    }
    else if( [avmode isEqualToString: @"audio"] )
    {
       Configs.Instance.avmode = AVModeAudio;
    }
    else if( [avmode isEqualToString: @"screen"] )
    {
        Configs.Instance.avmode = AVModeScreen;
    }
    else
    {
       logep( @"Unknown avmode value : %@", avmode );
    }
    
    //if( Configs.Instance.avmode != AVModeAudio )
    //{
    
    [VideoSessionFactory.Instance initialize:videoEventsListener];
    
    //}
    
    /*
     -(instancetype) initWithHandlers:(void(^)(void))onsuccess
     onfail:(void(^)(NSError * _Nullable))onfail
     onDisconnect:( void(^)(void) )onDisconnect
     onReconnect:( void(^)(void) )onReconnect
     onReconnectableEnd:( void(^)(void) )onReconnEnd
     onEnd:(void(^)(enum EndType, NSString*))onEnd;
     */
    
    [rtcSession setStartParamsWithOffer:NO];
    
    BOOL ret = [rtcSession start:sig forSession:sid eventsHandler:
     
            [[RTCEventsHandler alloc]
                            initWithHandlers:^{
                                logd( @"AV Session session started successfully" );
                                onComplete( [NSArray arrayWithObjects:[NSNumber numberWithBool:YES], nil] );
                            }
                            onfail:^(NSError * _Nullable error) {
                                logw( @"Could not connect audio session" );
                                onComplete( [NSArray arrayWithObjects: [NSNumber numberWithBool:NO], [error description], nil] );
                            }
                            onDisconnect:^{
                                 [self sendEventWithName:@"ondisconnect" body:@{} ];
                            }
                            onReconnect:^{
                                 [self sendEventWithName:@"onreconnect" body:@{} ];
                            }
                            onReconnectableEnd:^{
                                [self sendEventWithName:@"onreconnend" body:@{} ];
                            }
                            onEnd:^(enum EndType endType, NSString * desc) {
                                
                                [NSNotificationCenter.defaultCenter removeObserver:self];
                                
                                logip( @"End of audio session with desc : %@", desc );
                                [self sendEventWithName:@"onend"
                                                   body:@{
                                                          @"type":[RTCEventsHandler endTypeToStr:endType],
                                                          @"desc": desc
                                                    }
                                 ];
                            }
             ]
     ];
    
    if( ! ret )
    {
        onComplete( [NSArray arrayWithObjects: [NSNumber numberWithBool:NO], @"StartAvInitError", nil] );
    }
}


RCT_EXPORT_METHOD(tryReconnect)
{
    [rtcSession reNegotiate];
}


RCT_EXPORT_METHOD(getDeviceInfo:(RCTResponseSenderBlock)cb)
{
    cb( [NSArray arrayWithObjects:[DeviceInfo getInfo], nil] );
}


RCT_EXPORT_METHOD(passSigMessage:(NSString*)message)
{
    if( rtcSession == nil )
    {
        logw( @"RTCSession is nil, sigmessage ignored" );
        return;
    }
    
    [rtcSession onSigMessage: message];
}


RCT_EXPORT_METHOD(endav:(NSString*)reason)
{
    [NSNotificationCenter.defaultCenter removeObserver:self];
    
    if( rtcSession == nil )
    {
        logwp( @"RTCSession is nil, endav with reason '%@', ignored", reason );
        return;
    }
    
    [rtcSession end:reason];
    rtcSession = nil;
}


RCT_EXPORT_METHOD(setConfig:(NSString*)configs)
{
    NSData *data = [configs dataUsingEncoding:NSUTF8StringEncoding];
    
    if( g_AppConfigs != nil )
    {
        [[Configs Instance] setPauseAudioSoundFile: [g_AppConfigs pauseMusicFile] ];
    }
    
    NSError* err;
    NSDictionary* dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:(&err) ];
    
    if( err )
    {
        logep( @"Sigmessage is in invalid JSON format, %@", [err description]  );
        return;
    }
    
    [[Configs Instance] configByJSON:dict];
}


RCT_EXPORT_METHOD(pause)
{
    if( rtcSession == nil )
    {
        logw( @"RTCSession is nil, pause request ignored" );
        return;
    }
    
    [rtcSession pause:YES];
}

RCT_EXPORT_METHOD(resume)
{
    if( rtcSession == nil )
    {
        logw( @"RTCSession is nil, resume request ignored" );
        return;
    }
    
    [rtcSession resume];
}

RCT_EXPORT_METHOD(muteMic:(BOOL)enable)
{
    if( rtcSession == nil )
    {
        logwp( @"RTCSession is nil, muteMic(%d) request ignored", enable );
        return;
    }
    
    [rtcSession muteMic:enable];
}

RCT_EXPORT_METHOD(speakerPhone:(BOOL)enable)
{
    if( rtcSession == nil )
    {
        logwp( @"RTCSession is nil, speakerPhone(%d) request ignored", enable );
        return;
    }
    
    [rtcSession changeSpeaker:enable];
}

RCT_EXPORT_METHOD(playAudio)
{
    [AudioPlay createAndPlay];
}


RCT_EXPORT_METHOD(stopAudio)
{
    [AudioPlay stopAndDestroy];
}

static RNLivechatav* s_logInstance = nil;

static void __log( NSString *format, ...)
{
    va_list li;
    va_start(li, format);
    NSString* time = va_arg(li, NSString*);
    NSString* type = va_arg(li, NSString*);
    NSString* sid = va_arg(li, NSString*);
    NSString* class = va_arg(li, NSString*);
    NSString* msg = va_arg(li, NSString*);
    NSDictionary* dict = @{ @"type":type, @"class":class, @"sid":sid, @"msg":msg, @"time":time };
    [s_logInstance sendEventWithName:@"logmsg" body:dict ];
    va_end(li);
}


RCT_EXPORT_METHOD(enableLogEvents:(BOOL)enable)
{
    s_logInstance = self;
    if(enable)
        set_livechat_logger( __log , YES);
    else
        set_livechat_logger( NSLog , NO);
}


RCT_EXPORT_METHOD(pauseVideo:(BOOL)includeRemote byUser:(BOOL)byUser )
{
    if( rtcSession == nil )
    {
        logwp( @"RTCSession is nil, pauseVideo(%d) request ignored", includeRemote );
        return;
    }
    [rtcSession pauseVideoWithRemote: includeRemote byUser:byUser];
}


RCT_EXPORT_METHOD(resumeVideo:(BOOL)includeRemote byUser:(BOOL)byUser)
{
    if( rtcSession == nil )
    {
        logwp( @"RTCSession is nil, resumeVideo(%d) request ignored", includeRemote );
        return;
    }
    [rtcSession resumeVideoWithRemote: includeRemote byUser:byUser];
}

RCT_EXPORT_METHOD(echo:(NSString*)msg cb:(RCTResponseSenderBlock)cb)
{
    cb([NSArray arrayWithObjects:[msg stringByAppendingString:@" received"], nil]);
}


RCT_EXPORT_METHOD(getLibVersion:(RCTResponseSenderBlock)cb)
{
    cb( [NSArray arrayWithObject:[Version getVersionStr]] );
}


RCT_EXPORT_METHOD(getui:(RCTResponseSenderBlock)cb )
{
    UIButton* btn = [UIButton buttonWithType:UIButtonTypeSystem];
    [btn setTitle:@"Hello" forState:UIControlStateNormal];
    UIView* view = btn;
    cb( [NSArray arrayWithObject:view] );
}

@end

