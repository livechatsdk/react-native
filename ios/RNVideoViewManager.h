//
//  RNVideoViewManager.h
//  RNLivechatav
//
//  Created by Nuwan Abeysinghe on 31/12/18.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <React/RCTViewManager.h>

NS_ASSUME_NONNULL_BEGIN

@interface RN_VideoViewManager : NSObject

- (UIView *)createView;
-(instancetype)init;
+(instancetype)Instance;
-(int)addVideoView:(UIView*)view type:(NSString*)type;
-(void)viewIndexProperty:(id)json view:(UIView*)view;
-(int)removeVideoView:(UIView*)view;

@end

NS_ASSUME_NONNULL_END
