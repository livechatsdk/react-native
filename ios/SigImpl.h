//
//  SigImpl.h
//  RNLivechatav
//
//  Created by Nuwan Abeysinghe on 14/9/18.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <Signalling.h>

@interface SigImpl : NSObject<Signalling>
{
}

-(instancetype) initWithOnMsgBlk:( void(^)(NSString* message) )onMsg;

-(void)sendSigMessage:(NSString*_Nonnull)message;

-(void)rtcClose;

@end

