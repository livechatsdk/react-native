package com.reactlibrary;

import android.content.Context;
import android.graphics.Color;
import android.view.SurfaceView;
import android.view.ViewParent;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.react.views.image.ReactImageManager;

import sdk.livechat.Log;


public class RNVideoView extends RelativeLayout
{
    SurfaceView surfaceview;
    ReactImageManager img;

    static int viewCount = 0;
    int logid = 0;

    public RNVideoView(Context context)
    {
        super(context);
        logid = viewCount++;
        Log.i( "RNVideoView(" + (logid) + ") created" );
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        Log.i( "(" + logid + ") onlayout changed=" + changed + " l=" + l + " t=" + t + " r=" + r + " b=" + b  );
        super.onLayout( changed, l, t, r, b );
    }

    /*
    private void addChild()
    {
        TextView tv = new TextView( this.getContext() );
        tv.setText( "Hello.. !" );

        RelativeLayout.LayoutParams rlp = new RelativeLayout.LayoutParams(
                LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT
        );

        rlp.addRule( RelativeLayout.CENTER_IN_PARENT );
        tv.setLayoutParams( rlp );
        this.addView( tv );
        Log.i( "(" + logid + ") Child added" );
    }
    */


    public void setSurfaceView( SurfaceView view )
    {
        if( surfaceview != null )
        {
            Log.w( "(" + logid + ") a surfaceview already exist, replacing" );
        }

        surfaceview = view;
        RelativeLayout.LayoutParams rlp = new RelativeLayout.LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT
        );

        rlp.addRule( RelativeLayout.CENTER_IN_PARENT );
        //surfaceview.setLayoutParams( rlp );

        ViewParent vp = surfaceview.getParent();
        if( (vp != null) && (vp instanceof RNVideoView) )
        {
            ((RNVideoView)vp).removeView( surfaceview );
        }

        this.addView( surfaceview );
        //((SurfaceViewRenderer)surfaceview).setScalingType( RendererCommon.ScalingType.SCALE_ASPECT_FIT );

        Log.i( "(" + logid + ") Surfaceview added" );
    }


    private final Runnable measureAndLayout = new Runnable() {
        @Override
        public void run() {
            measure(
                    MeasureSpec.makeMeasureSpec(getWidth(), MeasureSpec.EXACTLY),
                    MeasureSpec.makeMeasureSpec(getHeight(), MeasureSpec.EXACTLY));
            layout(getLeft(), getTop(), getRight(), getBottom());
        }
    };

    @Override
    public void requestLayout() {
        super.requestLayout();

        // The spinner relies on a measure + layout pass happening after it calls requestLayout().
        // Without this, the widget never actually changes the selection and doesn't call the
        // appropriate listeners. Since we override onLayout in our ViewGroups, a layout pass never
        // happens after a call to requestLayout, so we simulate one here.
        post(measureAndLayout);
    }

}
