
//import AsyncStorage from '@react-native-community/async-storage';

import AsyncStorage from '@react-native-async-storage/async-storage';

export const storeItem = async (key, item) => {
  console.log('### LocalStorageUtils :: storeItem');
  try {
    var item1 = await AsyncStorage.setItem(key, item);
    return item1;
  } catch (error) {
    console.log(
      `### LocalStorageUtils :: storeItem :: AsyncStorage error: ${error.message}`,
    );
  }
};

export const retrieveItem = async key => {
  console.log('### LocalStorageUtils :: retrieveItem');
  try {
    const retrievedItem = await AsyncStorage.getItem(key);
    return retrievedItem;
  } catch (error) {
    console.log(
      `### LocalStorageUtils :: retrieveItem :: AsyncStorage error: ${error.message}`,
    );
  }
  return;
};

export const clear = async () => {
  console.log('### LocalStorageUtils :: retrieveItem');
  try {
    await AsyncStorage.clear();
    return true;
  } catch (error) {
    console.log(
      `### LocalStorageUtils :: clearStorage :: AsyncStorage error: ${error.message}`,
    );
    return false;
  }
};

export const getPersistenceData = async () => {
  console.log('### LocalStorageUtils :: getPersistenceData');
  let persistenceData = '';
  try {
    persistenceData = await AsyncStorage.getItem('persistenceData');
    if (persistenceData !== null) {
      persistenceData = await JSON.parse(persistenceData);
      console.log(
        'getPersistenceData :: persistenceData ::\n',
        persistenceData,
      );
    }
  } catch (error) {
    console.log(
      `### LocalStorageUtils :: getPersistenceData :: AsyncStorage error: ${error.message}`,
    );
  }

  return persistenceData;
};

export const savePersistenceData = async data => {
  console.log(
    '### LocalStorageUtils :: saveSessionData :: data - Before',
    data,
  );
  console.log(
    '### LocalStorageUtils :: savePersistenceData ',
    JSON.stringify(data),
  );
  try {
    await AsyncStorage.setItem('persistenceData', JSON.stringify(data));
    console.log(
      'persistenceData ::\n',
      await AsyncStorage.getItem('persistenceData'),
    );
  } catch (error) {
    console.log(
      `### LocalStorageUtils :: savePersistenceData :: AsyncStorage error:${error.message}`,
    );
  }
};
